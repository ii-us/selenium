# Selenium

The english version of the following instruction ca be found [here](README_EN.md).

## Instalacja

Przykładowy kod został napisany w JavaScript-cie, do uruchomienia wymaga zainstalowanego `node.js`.
Niezbędne biblioteki instalujemy przy pomocy `npm install`.

Do działania kod wymaga jeszcze `chromedriver`, którego można pobrać z https://chromedriver.chromium.org/ (uwaga! należy pobrać wersje dla swojego Chrome-a). Program musi być dostępny w zmiennej środowiskowej `PATH`.

## Uruchomienie

Testy można uruchomić za pomocą komendy `npm test`.

## Zadania do wykonania

1. Stwórz test kalkulatora znajdującego się na stronie http://www.anaesthetist.com/mnm/javascript/calc.htm.
2. Wykonaj dowolne mnożenie a wykorzystując asercję sprawdź wynik.
3. Rozszerz test o pobranie zrzutu strony, dokumentacja https://www.selenium.dev/selenium/docs/api/javascript/module/selenium-webdriver/index_exports_WebDriver.html#takeScreenshot.